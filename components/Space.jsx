const Index = ({ size }) => {
    return (
        <>
            <div
                className="width-p-100"
                style={{ height: `${size / 16}rem` }}
            ></div>
        </>
    );
};
export default Index;
