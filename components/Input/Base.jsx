import { useState } from "react";
import * as Yup from "yup";
import { toast } from "react-toastify";

import log from "@/functions/log";

const Index = ({
    yup = Yup.string(),
    label = "",
    placeholder = "",
    defaultValue = null,
    value = null,
    type = "text",
    classNameLabel = "",
    classNameInput = "",
    classNameIcon = "",
    classNameError = "",
    onChange = () => {},
    onBlur = () => {},
    onChangeValidate = (e) => e,
    props = {},
    icon = <></>,
}) => {
    const [statusInput, setStateInput] = useState("");
    const [error, setError] = useState("");
    const [valueInput, setValueInput] = useState(defaultValue ?? "");

    const validateValue = (v) => {
        v = onChangeValidate(v);
        yup.validate(v)
            .then(function (valid) {
                if (valid) {
                    setStateInput("ok");
                    setError("");
                }
            })
            .catch(function (error) {
                log("error", error, "red");
                setStateInput("error");
                setError(error.message);
            });
        return v;
    };

    const changeInput = (e) => {
        const v = validateValue(e.target.value);
        setValueInput(v);
        onChange(v);
    };
    const blurInput = () => {
        validateValue(valueInput);
        onBlur();
    };
    return (
        <>
            <label className={classNameLabel}>
                <div>{label}</div>
                <div>
                    <input
                        type={type}
                        className={`input ${classNameInput} ${statusInput}`}
                        placeholder={placeholder}
                        value={value ?? valueInput}
                        onChange={changeInput}
                        onBlur={blurInput}
                        {...props}
                    />
                    <span className={classNameIcon}>{icon}</span>
                </div>
                {error != "" && <div className={classNameError}>{error}</div>}
            </label>
        </>
    );
};
export default Index;
