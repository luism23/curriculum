import InputBase from "@/components/Input/Base"

const Index = (props) => {
    const config = {...props}
    config.classNameInput = `
        p-h-10 p-v-5 
        font-20 font-montserrat
        width-p-100 
        border-0 
        border-radius-0
        box-shadow box-shadow-inset box-shadow-s-1 box-shadow-black 
        outline-none 
    `
    config.classNameLabel = `
        font-20 font-montserrat
    `
    config.classNameError = `
        font-10 font-montserrat
        text-right
        p-v-3
        color-error
    `
    return (
        <>
            <InputBase
                {...config}
            />
        </>
    )
}
export default Index