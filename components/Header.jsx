import { useState, useEffect, useRef } from "react";
import DATA from "@/data/components/Header"

import data from "@/data/components/Header"


import Bg from "@/components/Bg"

const Index = () => {
    const { img, imgLogo, title, text } = data
    const header = useRef(null);

    const loadHeightTopPaddingBody = () => {
        const height = header.current.offsetHeight;
        document.body.style.paddingTop = `${height}px`;
    };
    const loadHeader = () => {
        // loadHeightTopPaddingBody();
    };
    useEffect(() => {
        loadHeader();
    }, []);
    return (
        <>
            <header
                ref={header}
                className="header pos-r  top-0 left-0 width-p-100 height-125 flex flex-align-center p-v-5"
            >
                <Bg
                    src={img}
                    className=""
                />
                <div className="container p-h-15 flex">


                    <div className="flex flex-justify-center">
                        <div className="flex-4">
                            <img className="d-block border-radius-60 width-80" src={`/image/${imgLogo}`} alt="" />
                        </div>
                        <div className="flex-8">
                            <h1 className="font-poppins font-w-800 font-36 color-white">
                                {title}
                            </h1>
                            <span className="font-poppins font-18 color-white">
                                {text}
                            </span>
                        </div>

                    </div>
                    <div>

                    </div>

                </div>
            </header>
        </>
    );
};
export default () => (<Index {...DATA} />)
