import Input from "@/components/Input";
import Select from "@/components/Select";
import Content from "@/components/Content";

import VALIDATORINPUTS from "@/data/validations/inputs";

const Index = () => {
    return (
        <>
            <Content title="Inputs">
                <div className="container p-15 flex flex-justify-between  flex-gap-15">
                    <div className="flex-12 flex-md-6 flex-lg-4 flex-xl-3 flex-gap-15">
                        <Input label="Name" yup={VALIDATORINPUTS.name} />
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-4 flex-xl-3 flex-gap-15">
                        <Input
                            label="Email"
                            type="email"
                            yup={VALIDATORINPUTS.email}
                        />
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-4 flex-xl-3 flex-gap-15">
                        <Input
                            label="Web Site"
                            type="url"
                            yup={VALIDATORINPUTS.url}
                        />
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-4 flex-xl-3 flex-gap-15">
                        <Input
                            label="Number"
                            type="number"
                            yup={VALIDATORINPUTS.number}
                        />
                    </div>
                    <div className="flex-12 flex-md-6 flex-lg-4 flex-xl-3 flex-gap-15">
                        <Select
                            label="Options"
                            yup={VALIDATORINPUTS.options}
                            options={[
                                {
                                    text: "Option 1",
                                    value: "1",
                                },
                                {
                                    text: "Option 2",
                                    value: "2",
                                },
                                {
                                    text: "Option 3",
                                    value: "3",
                                },
                                {
                                    text: "hola 3",
                                    value: "3",
                                },
                                {
                                    text: "ppppp 3",
                                    value: "3",
                                },
                                {
                                    text: "rrrr 3",
                                    value: "3",
                                },
                            ]}
                        />
                    </div>
                </div>
            </Content>
        </>
    );
};
export default Index;
