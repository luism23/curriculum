export default {
    name: {
        required: "Name is Required",
    },
    email: {
        required: "Email is Required",
        invalid: "Email invalid",
    },
    url: {
        required: "Web Site is Required",
        invalid: "Web Site Invalid",
    },
    number: {
        required: "Number is Required",
        invalid: "Number Invalid",
        min: "Number is short",
        max: "Number is long",
    },
    options: {
        required: "Option is Required",
    },
};
