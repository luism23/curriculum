import * as Yup from "yup";

import DATAERROR from "@/data/error/inputs";

export default {
    name: Yup.string().required(DATAERROR.name.required),
    email: Yup.string()
        .email(DATAERROR.email.invalid)
        .required(DATAERROR.email.required),
    url: Yup.string()
        .url(DATAERROR.url.invalid)
        .required(DATAERROR.url.required),
    number: Yup.string(DATAERROR.number.invalid)
        .required(DATAERROR.number.required)
        .min(10, DATAERROR.number.min)
        .max(20, DATAERROR.number.max),
    options: Yup.object().shape({
        value: Yup.string().required(DATAERROR.options.required),
    }),
};
